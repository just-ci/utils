# [1.2.0](https://gitlab.com/just-ci/utils/compare/v1.1.0...v1.2.0) (2022-09-13)


### Features

* disable success comment ([88a8d38](https://gitlab.com/just-ci/utils/commit/88a8d38987007c12ad0097512f49d11c1b11efdb))

# [1.1.0](https://gitlab.com/just-ci/utils/compare/v1.0.0...v1.1.0) (2022-08-04)


### Features

* semantic release auto assets ([d763695](https://gitlab.com/just-ci/utils/commit/d7636959cc523e5245a61507568fb90002a2e366))
